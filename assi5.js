// 5.You are an agent and you need to find the total number of ways a message can bedecoded. 
// Any message is encoded as follows:A -1, B - 2, C - 3......Z - 26.
// An empty digit string will have only single decoding. It may be assumedthat the input contains valid digits from 0 to 9 and If there are leading 0’s,extra trailing 0’s and two or more consecutive 0’s then it is an invalidstring.
// Example - 123 as input can be decoded as “ABC” (1 2 3), “LC” (12 3) or“AW” (1 23). 
//So total ways are 3 which will be the output.

function countDecoding(digits, n)
    {
        if (n == 0 || n == 1)
        {
            return 1;
        }
        if (digits[0] == '0')
        {
            return 0;
        }
         
        let count = 0;
        if (digits[n - 1] > '0')
        {
            count = countDecoding(digits, n - 1);
        }
        if (digits[n - 2] == '1'
            || (digits[n - 2] == '2'
                && digits[n - 1] < '7'))
        {
            count += countDecoding(digits, n - 2);
        }
        return count;
    }
    function countWays(digits, n)
    {
        if (n == 0 || (n == 1 && digits[0] == '0'))
        {
            return 0;
        }
        return countDecoding(digits, n);
    }
    let len=3;
    let digits=[1,2,3]
    console.log(countWays(digits,len))