// 6.Given a string S. Write a code to print all possible permutations of the string.
//  Also findthe first non repeating character in this string.
// Example - S = “hiokjholiw” output = k

function getPermutations(string) {
    var results = [];

    if (string.length === 1) 
    {
      results.push(string);
      return results;
    }

    for (var i = 0; i < string.length; i++) 
    {
      var firstChar = string[i];
      var otherChar = string.substring(0, i) + string.substring(i + 1);
      var otherPermutations = getPermutations(otherChar);
      
      for (var j = 0; j < otherPermutations.length; j++) {
        results.push(firstChar + otherPermutations[j]);
      }
    }
    return results;
  }

  function firstNonRepeatingCharacter(str) {
    for (let i = 0; i < str.length; i++) {
      let char = str[i];
      if (str.indexOf(char) == i && str.indexOf(char, i + 1) == -1) {
        return char;
      }
    }
    return "No repeating character";
  }

console.log(getPermutations("abc"));
console.log(firstNonRepeatingCharacter("hiokjholiw"));