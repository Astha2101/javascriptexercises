// 3.Given a boolean 2D array where each row is sorted. 
// Find the row with the maximumnumber of ​1s​.
// Your input must be :-First line containing m and n where m is no of rows and n is no ofcolumns.
// -Second line containing the array elements.
//Example - Input 4 4 0 1 1 1 0 0 1 1 1 1 1 1 0 0 0 0
// Output - 2 , as row 2 has max no of 1’s (zero based indexing)

const prompt = require('prompt-sync')({sigint: true});

function maxOnesRow(matrix,row,column)
{
   var maxOne=0;
   var rowIndex=0;
   for(var i=0;i<row;i++)
   {
       var countOne=0
       for(var j=0;j<column;j++)
       {
           if(matrix[i][j]==1) countOne++;
       }
       if(countOne>maxOne)
       {
           maxOne=countOne;
           rowIndex=i;
       }
   }
   return rowIndex;
}

let row=4;
let column=4;
var matrix=new Array(row)
for(var i=0;i<row;i++)
{
    matrix[i]=new Array(column);
    for(var j=0;j<column;j++)
    {
        var input=prompt("Enter value :")
        matrix[i][j]=input;
    }
}
console.log(maxOnesRow(matrix,row,column))