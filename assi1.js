// 1.Write a JS code to find whether two strings are anagram of each other.
//​​An anagram ofa string is another string that contains the same characters, only the order of characterscan be different. 
//For example, “abcd” and “dabc” are anagram of each other. Similarly“listen” and “Silent”. Note- do not consider the case of letters in words.


function checkAnagram(a, b) {
    let len1 = a.length;
    let len2 = b.length;
    if(len1 !== len2){
       console.log('Invalid Input');
       return
    }
    let str1 = a.toLowerCase().split('').sort().join('');
    let str2 = b.toLowerCase().split('').sort().join('');
    console.log(str1)
       console.log(str2) 
    if(str1 === str2){
       console.log("True");
    } else { 
       console.log("False");
    }
 }
 checkAnagram("listen","Slient")
 checkAnagram("abcd","dbce")