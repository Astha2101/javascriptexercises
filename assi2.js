// 2.Write a function in JS that takes a string consisting of only round brackets as an inputand checks if the bracket sequence is regular or not. ​
// A bracket sequence is called regular if it is possible to insert some numbers and signs into the sequence in such away that the new sequence will represent a correct arithmetic expression.
//Example: “((()))” should return true as it is a regular bracket sequence but“()(()” and “)(“ should return false.


function regularBracket(brackets)
{
    let stack=[]
    for(var i=0;i<brackets.length;i++)
    {
        if(brackets[i]=='(')
        {
            stack.push('(');
        }
        else if(brackets[i]==')' && stack.length==0) 
        {
            return false;
        }
        else
        {
           stack.pop()
        }
    }
    if(stack.length==0) return true;
    return false;
}

console.log(regularBracket("((()))"))
console.log(regularBracket("()(()"))
console.log(regularBracket(")("))