// 4.Given two strings ​str1​ and ​str2​ and below operations that can be performed on str1.
// Find the minimum number of edits (operations) required to convert ‘str1′ into ‘str2′.
// -Insert, remove , replaceAll of the above operations are of cost=1. 
// Both the strings are oflowercase.
// Example - str1 = “peek” str2 = “peeks” then output = 1 as one operation isrequired to make 2 strings the same i.e. removing 's' from str2.